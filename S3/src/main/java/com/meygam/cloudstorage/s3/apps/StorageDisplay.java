package com.meygam.cloudstorage.s3.apps;

/**
 * Created by Elizaebth on 5/7/2014.
 */
public class StorageDisplay {
    private String accessKey;
    private String secretKey;
    private String description;
    private String provider;
    private String numBuckets;

    public StorageDisplay(String accessKey, String secretKey, String description, String provider,
                          String numBuckets) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.description = description;
        this.provider = provider;
        this.numBuckets = numBuckets;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumBuckets() {
        return numBuckets;
    }

    public void setNumBuckets(String numBuckets) {
        this.numBuckets = numBuckets;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
