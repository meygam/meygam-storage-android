package com.meygam.cloudstorage.s3.apps;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.model.S3Object;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;


public class S3BucketViewActivity extends CustomListActivity
        implements SwipeRefreshLayout.OnRefreshListener {

    protected List<String> objectNameList;
    protected String accountName;
    protected String bucketName;
    protected String folderName;
    protected String prefix;
    private boolean objectClicked = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        Bundle extras = this.getIntent().getExtras();
        accountName = extras.getString(S3.ACCOUNT_NAME);
        bucketName = extras.getString(S3.BUCKET_NAME);
        prefix = extras.getString(S3.PREFIX);
        folderName = extras.getString(S3.FOLDER_NAME);
        startPopulateList();
    }

    protected void obtainListItems(){
        new GetObjectNamesForBucketTask().execute();
    }

    protected void obtainMoreItems(){
        new GetMoreObjectNamesForBucketTask().execute();
    }

    protected void wireOnListClick(){
        getItemList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> list, View view, int position, long id) {
                final String objectName = ((TextView) view).getText().toString();
                if (objectName.endsWith("/")) {
                    // Is a folder
                    prefix = objectName;
                    folderName = folderName + objectName;
                    Intent bucketViewIntent = new Intent(S3BucketViewActivity.this,
                            S3BucketViewActivity.class);
                    bucketViewIntent.putExtra(S3.ACCOUNT_NAME, accountName);
                    bucketViewIntent.putExtra(S3.BUCKET_NAME, bucketName);
                    bucketViewIntent.putExtra(S3.PREFIX, prefix);
                    bucketViewIntent.putExtra(S3.FOLDER_NAME, folderName);
                    startActivity(bucketViewIntent);
                    overridePendingTransition(R.anim.animation_slide_in_left,
                            R.anim.animation_slide_out_left);
                } else if (!objectName.contains("is empty")) {
                    objectClicked = true;
                    // Is an object
                    Intent objectViewIntent = new Intent(S3BucketViewActivity.this, S3ObjectViewActivity.class);
                    objectViewIntent.putExtra(S3.ACCOUNT_NAME, accountName);
                    objectViewIntent.putExtra(S3.BUCKET_NAME, bucketName);
                    objectViewIntent.putExtra(S3.OBJECT_NAME, objectName);

                    if (folderName.equals(""))
                        objectViewIntent.putExtra(S3.FOLDER_NAME, objectName);
                    else
                        objectViewIntent.putExtra(S3.FOLDER_NAME, folderName + objectName);
                    startActivity(objectViewIntent);
                    overridePendingTransition(R.anim.animation_slide_in_left,
                            R.anim.animation_slide_out_left);
                }
            }
        });

        getItemList().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final String objectName = ((TextView) view).getText().toString();
                SharedPreferences sharedPreferences = getApplicationContext()
                        .getSharedPreferences("myPrefs", MODE_PRIVATE);
                String defaultValue = getApplicationContext().getResources()
                        .getString(R.string.default_download_dir);
                String downloadDir = sharedPreferences.getString(SettingsActivity.KEY_DOWNLOAD_DIR,
                        defaultValue);

                if (!objectName.endsWith("/") && !objectName.contains("is empty")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(S3BucketViewActivity.this);
                    builder.setTitle("Download")
                            .setIcon(R.drawable.ic_download)
                            .setMessage("Do you want to download the file to "
                                    + downloadDir +
                                    "? To change the download directory, use Settings menu.")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    new GetObjectContent().execute(objectName);
                                }
                            })
                            .setNegativeButton("No", null);
                    builder.create().show();
                }

                return true;
            }
        });
    }

    private String goUpOneLevel(String folderName) {
        String toRet = "";
        if(folderName != null && folderName.length() > 0) {
            String[] splitStr = folderName.split("/");

            for (int i = 0; i < splitStr.length - 1; i++) {
                toRet = toRet + splitStr[i] + "/";
            }

        }
        return toRet;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(objectClicked == false) {
            folderName = goUpOneLevel(folderName);
        } else {
            objectClicked = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_slide_in_right,
                R.anim.animation_slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.s3_bucket_list, menu);
        return true;
    }

    public boolean onSettingsClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(nextActivity);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                onSettingsClicked(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                startPopulateList();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    private class GetObjectContent extends AsyncTask<String, String, String> {
        SharedPreferences sharedPreferences = getApplicationContext()
                .getSharedPreferences("myPrefs", MODE_PRIVATE);
        String defaultValue = getApplicationContext().getResources()
                .getString(R.string.default_download_dir);
        String downloadDir = sharedPreferences.getString(SettingsActivity.KEY_DOWNLOAD_DIR,
                defaultValue);
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(S3BucketViewActivity.this);
            progressDialog.setMessage("Downloading file. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String objectName = strings[0];

            InputStream inputStream;
            long lengthOfFile = 0;
            S3Object s3Object;
            try {
                if (folderName.equals("")) {
                    s3Object = S3.getS3Object(accountName, bucketName, objectName);
                    inputStream = s3Object.getObjectContent();
                    lengthOfFile = s3Object.getObjectMetadata().getContentLength();
                } else {
                    s3Object = S3.getS3Object(accountName, bucketName, folderName + objectName);
                    inputStream = s3Object.getObjectContent();
                    lengthOfFile = s3Object.getObjectMetadata().getContentLength();
                }
            } catch (final AmazonClientException exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder
                                = new AlertDialog.Builder(S3BucketViewActivity.this);
                        builder.setTitle("Amazon Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }

            byte[] buffer = new byte[1024];
            try {
                OutputStream outputStream
                        = new FileOutputStream(new File(downloadDir + "/" + objectName));
                int count = 0;
                long total = 0;
                while ((count = inputStream.read(buffer)) != -1) {
                    total+= count;
                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }
                    publishProgress(""+(int)((total*100)/lengthOfFile));
                    outputStream.write(buffer, 0, count);
                }
                outputStream.close();
                inputStream.close();
                UtilFunctions.sendEventAnalytics(getApplicationContext(),
                        "Meygam Storage - S3BucketViewActivity",
                        "Download Object",
                        "Completed");

            } catch (final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(S3BucketViewActivity.this);
                        builder.setTitle("Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }

            return objectName;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String string) {
            progressDialog.dismiss();
            Toast.makeText(S3BucketViewActivity.this,
                    string + " downloaded to " + downloadDir,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private class GetObjectNamesForBucketTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(S3BucketViewActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        protected Void doInBackground(Void... voids) {
            try {
                if (folderName.equals(""))
                    objectNameList = S3.getObjectNamesForBucket(accountName, bucketName, prefix);
                else
                    objectNameList = S3.getObjectNamesForBucket(accountName, bucketName, folderName);
            } catch (final AmazonClientException exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(S3BucketViewActivity.this);
                        builder.setTitle("Amazon Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
            updateUi(objectNameList);
        }
    }

    private class GetMoreObjectNamesForBucketTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... voids) {
            objectNameList = S3.getMoreObjectNamesForBucket(accountName);
            return null;
        }

        protected void onPostExecute(Void result) {
            updateList(objectNameList);
        }
    }
}
