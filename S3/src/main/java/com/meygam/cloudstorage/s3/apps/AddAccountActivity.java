package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

public class AddAccountActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_account);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        final EditText accessKeyEditText = (EditText) findViewById(R.id.accessKey);
        final TextView accessKeyError = (TextView) findViewById(R.id.accessKeyError);
        final EditText secretKeyEditText = (EditText) findViewById(R.id.secretKey);
        final TextView secretKeyError = (TextView) findViewById(R.id.secretKeyError);
        final EditText descriptionEditText = (EditText) findViewById(R.id.description);
        final TextView descriptionError = (TextView) findViewById(R.id.descriptionError);


        final Button addAccountButton = (Button) findViewById(R.id.addAccountButton);
        addAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean validationSuccess = true;

                if(accessKeyEditText.getText().toString().equals("")) {
                    accessKeyError.setVisibility(View.VISIBLE);
                    validationSuccess = false;
                }

                if(secretKeyEditText.getText().toString().equals("")) {
                    secretKeyError.setVisibility(View.VISIBLE);
                    validationSuccess = false;
                }

                if(descriptionEditText.getText().toString().equals("")) {
                    descriptionError.setVisibility(View.VISIBLE);
                    validationSuccess = false;
                }

                if(validationSuccess) {
                    MainActivity.clientManager.setCredentials(accessKeyEditText.getText().toString(),
                            secretKeyEditText.getText().toString(),
                            descriptionEditText.getText().toString());

                    String validateCredentials
                            = S3.validateCredentials(descriptionEditText.getText().toString());
                    if(!validateCredentials.equals("success")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddAccountActivity.this);
                        builder.setTitle("Invalid Credentials")
                                .setIcon(R.drawable.ic_alert_icon)
                                .setMessage(validateCredentials
                                        +"\n\nCheck your access credentials and try again.")
                                .setPositiveButton("OK", null);
                        builder.create().show();
                        return;
                    }

                    AccountManager.getInstance().addAccount(getApplicationContext(),
                            accessKeyEditText.getText().toString(),
                            secretKeyEditText.getText().toString(),
                            descriptionEditText.getText().toString(),
                            "Amazon");

                    Toast.makeText(getApplicationContext(),
                            "Account added to Cloud Storage Dashboard",
                            Toast.LENGTH_LONG).show();
                    AddAccountActivity.this.finish();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
}
