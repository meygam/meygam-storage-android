package com.meygam.cloudstorage.s3.apps;

import android.content.Context;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;

import java.util.HashMap;

/**
 * Created by Elizabeth on 5/6/2014.
 */
public class AmazonClientManager {
    private HashMap<String, AmazonS3Client> s3ClientHashMap = new HashMap<String, AmazonS3Client>();

    private Context context = null;

    public AmazonClientManager(Context context) {
        this.context = context;
    }

    public void setCredentials(String accessKey, String secretKey, String description) {
        AWSCredentials credentials
                = new BasicAWSCredentials( accessKey, secretKey );
        AmazonS3Client s3Client = new AmazonS3Client( credentials );
        if(s3Client != null) {
            s3ClientHashMap.put(description, s3Client);
        }
    }

    public HashMap<String, AmazonS3Client> getS3ClientHashMap() {
        return s3ClientHashMap;
    }

    public AmazonS3Client s3(String description) {
        return s3ClientHashMap.get(description);
    }
}
