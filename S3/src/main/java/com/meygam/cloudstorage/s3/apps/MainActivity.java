package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;
import com.mobijibe.external.DeviceManager;

import java.util.ArrayList;


public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener{

    protected ListView s3ListView;
    protected SwipeRefreshLayout swipeRefreshLayout;

    public static AmazonClientManager clientManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.s3_browser);

        UtilFunctions.setAppVersion(getResources().getString(R.string.version));

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        s3ListView = (ListView) findViewById(R.id.main_listView);
        s3ListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if(s3ListView != null && s3ListView.getChildCount() > 0){
                    // check if the first item of the list is visible
                    boolean firstItemVisible = s3ListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = s3ListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeRefreshLayout.setEnabled(enable);
            }
        });

        clientManager = new AmazonClientManager(getApplicationContext());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        AccountList accountList = AccountManager.getInstance().readFromFile(getApplicationContext());
        if(accountList == null) {
            this.displayCredentialsIssue();
        } else {
            refreshAccountList(accountList);
        }

        // For mobisdk
        DeviceManager deviceManager = new DeviceManager(this.getApplicationContext());
        deviceManager.registerDevice();
    }

    private void refreshAccountList(AccountList accountList) {
        if(accountList == null) {
            UtilFunctions.sendEventAnalytics(getApplicationContext(),
                    "Meygam Storage - MainActivity",
                    "Current Number of Accounts",
                    "No accounts present");
            return;
        }
        ArrayList<StorageDisplay> storageDisplayArrayList = new ArrayList<StorageDisplay>();
        for(AccountDetails accountDetails : accountList) {
            MainActivity.clientManager.setCredentials(accountDetails.getAccessKey(),
                    accountDetails.getSecretKey(),
                    accountDetails.getDescription());

            storageDisplayArrayList.add(new StorageDisplay(accountDetails.getAccessKey(),
                    accountDetails.getSecretKey(), accountDetails.getDescription(),
                    accountDetails.getProvider(),
                    S3.getNumberOfBuckets(accountDetails.getDescription())));
        }

        s3ListView.setVisibility(View.VISIBLE);
        s3ListView.setAdapter(new StorageDisplayAdapter(this,
                android.R.layout.simple_list_item_1,
                storageDisplayArrayList));
        this.wireListView();
        UtilFunctions.sendEventAnalytics(getApplicationContext(),
                "Meygam Storage - MainActivity",
                "Current Number of Accounts",
                ""+accountList.size());
            //registerForContextMenu(s3ListView);

    }

    /*@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_delete, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }*/

    /*@Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }*/

    private void wireListView(){
        s3ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                StorageDisplay storageDisplay = (StorageDisplay) s3ListView.getAdapter()
                        .getItem(index);
                Intent nextActivity = new Intent(MainActivity.this, S3BucketListActivity.class);
                nextActivity.putExtra(S3.ACCOUNT_NAME, storageDisplay.getDescription());
                startActivity(nextActivity);
                overridePendingTransition(R.anim.animation_slide_in_left, R.anim.animation_slide_out_left);
            }
        });
        s3ListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int index, long l) {
                final StorageDisplay storageDisplay = (StorageDisplay) s3ListView.getAdapter()
                        .getItem(index);
                // Build the dialog to show edit/delete options
                AlertDialog.Builder deleteBuilder = new AlertDialog
                        .Builder(MainActivity.this);
                deleteBuilder.setTitle("Delete Account")
                        .setIcon(R.drawable.ic_delete_account)
                        .setMessage("Do you want to delete the account details? " +
                                "This action cannot be undone.")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                MainActivity.clientManager.getS3ClientHashMap()
                                        .remove(storageDisplay.getDescription());
                                AccountList accountList = AccountManager.getInstance()
                                        .removeAccount(getApplicationContext(),
                                        storageDisplay.getAccessKey(),
                                        storageDisplay.getSecretKey(),
                                        storageDisplay.getDescription(),
                                        storageDisplay.getProvider());
                                refreshAccountList(accountList);
                            }
                        });
                deleteBuilder.create().show();
                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshAccountList(AccountManager.getInstance().readFromFile(getApplicationContext()));
        overridePendingTransition(R.anim.animation_slide_in_right, R.anim.animation_slide_out_right);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refreshAccountList(AccountManager.getInstance().readFromFile(getApplicationContext()));
    }

    protected void displayCredentialsIssue() {
        AlertDialog.Builder confirm = new AlertDialog.Builder( this );
        confirm.setTitle("Credential Not Configured!");
        confirm.setMessage( "Please use Add Account to configure details" );
        confirm.setNegativeButton( "OK", new DialogInterface.OnClickListener() {
            public void onClick( DialogInterface dialog, int which ) {
                Intent nextActivity = new Intent(getApplicationContext(), AddAccountActivity.class);
                startActivity(nextActivity);
            }
        } );
        confirm.show().show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onAddAccountClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), AddAccountActivity.class);
        startActivity(nextActivity);
        return true;
    }

    public boolean onSettingsClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(nextActivity);
        return true;
    }

    public boolean onAboutClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), AboutActivity.class);
        startActivity(nextActivity);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.add_account:
                onAddAccountClicked(item);
                return true;
            case R.id.action_settings:
                onSettingsClicked(item);
                return true;
            case R.id.about:
                onAboutClicked(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                swipeRefreshLayout.setRefreshing(false);
                refreshAccountList(AccountManager.getInstance().readFromFile(getApplicationContext()));
            }
        }, 2000);
    }
}
