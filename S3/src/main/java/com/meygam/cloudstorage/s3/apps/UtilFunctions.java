package com.meygam.cloudstorage.s3.apps;

import android.content.Context;
import android.os.AsyncTask;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by Elizabeth on 5/23/2014.
 */
public class UtilFunctions {
    private static final String sender_emailName = "CloudStorage";
    //private static final String sender_emailAddress = "support@meygam.com";
    //private static final String sender_emailPassword = "T6nmh8Euyp8c";
    private static final String sender_emailAddress = "cloud.storage@meygam.com";
    private static final String sender_emailPassword = "9XwZLIGq9bxi";
    private static final String recipient_emailAddress = "support@meygam.com";

    private static final String subject_feedback = "Feedback from Cloud Storage User";
    private static final String subject_suggestingaFeature = "Suggesting a Feature";
    private static final String subject_reportaproblem = "Report a Problem";
    private static final String subject_exception = "Crash & Exception";
    private static final String PhoneModel = android.os.Build.MODEL;
    private static final String AndroidVersion = android.os.Build.VERSION.RELEASE;
    private static String about_version;

    public static void setAppVersion(String s)
    {
        about_version = s;
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.live.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender_emailAddress, sender_emailPassword);
            }
        });
    }

    private Message createMessage(String email, String subject, String messageBody, Session session)
            throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(sender_emailAddress, sender_emailName));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void sendMail(String email, String messageBody, String subject) {
        Session session = createSessionObject();

        try {
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void sendExceptionMail(String exceptionText)
    {
        UtilFunctions obj = new UtilFunctions();
        obj.sendMail(recipient_emailAddress, exceptionText, subject_exception);
    }

    public static boolean sendUserFeedback (String name, String email, String feedback) {
        UtilFunctions obj = new UtilFunctions();
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        String messagebody = "Feedback\n"
                + "Name: " + name + "\n"
                + "Email: " + email + "\n"
                + "Feedback: " + feedback + "\n"
                + "Device Model : " + PhoneModel + "\n"
                + "Android Version:" + AndroidVersion + "\n"
                + "App Version: " + about_version + "\n"
                + "Time zone: " + tz.getDisplayName() + "\n"
                + "DayLightSaving: " + tz.inDaylightTime(new Date(0));

        obj.sendMail(recipient_emailAddress, messagebody, subject_feedback);
        return true;
    }

    public static boolean sendFeatureSuggestion(String name, String email, String featureKind,
            String feature) {
        UtilFunctions obj = new UtilFunctions();
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        String messagebody = "Feature\n"
                + "Name: " + name + "\n"
                + "Email: " + email + "\n"
                + "feature: " + feature + "\n"
                + "Device Model : " + PhoneModel + "\n"
                + "Android Version:" + AndroidVersion + "\n"
                + "App Version: " + about_version + "\n"
                + "Time zone: " + tz.getDisplayName() + "\n"
                + "DayLightSaving: " + tz.inDaylightTime(new Date(0));

        obj.sendMail(recipient_emailAddress, messagebody, subject_suggestingaFeature);
        return true;
    }

    public static boolean sendFeedback(String reportMessage){
        UtilFunctions obj = new UtilFunctions();
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        String messagebody = "Feedback from user :\n"
                + reportMessage + "\n"
                + "Device Model : " + PhoneModel + "\n"
                + "Android Version:" + AndroidVersion + "\n"
                + "App Version: " + about_version + "\n"
                + "Time zone: " + tz.getDisplayName() + "\n"
                + "DayLightSaving: " + tz.inDaylightTime(new Date(0));

        obj.sendMail(recipient_emailAddress, messagebody, subject_feedback);
        return true;
    }

    public static void sendEventAnalytics(Context context, String eventCategory, String eventAction,
                                          String eventLabel) {
        EasyTracker easyTracker = EasyTracker.getInstance(context);
        easyTracker.send(MapBuilder.createEvent(
                        eventCategory, // Event category (required)
                        eventAction, // Event action (required)
                        eventLabel, // Event label
                        null) // Event value
                        .build()
        );
    }
}
